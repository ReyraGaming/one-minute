<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '724$a)FoL+>_D6*&DgOLp]X*+psmveW=W@aFmvsoA..&rjIQ XOXBeX.L ;kpo{!');
define('SECURE_AUTH_KEY',  'V*me60qr%ngVQ> |s><x?T|sZw4smf<+D2Nvda/Yw [mc?i*Kfh+_@vTsu9jG_:2');
define('LOGGED_IN_KEY',    '7PCP*<fdNuH]e4oL{bsiH0mVzDQ*rGj4I_[=)P2Pk$2d+ ^0$4;UY4`UcKtj.gGm');
define('NONCE_KEY',        'rEc6%+V..!9?AqWC`$(J>~g%/BI@A^AFHg>V,-z4z5y;GSC>Sb+,~_-g]B1J:#FX');
define('AUTH_SALT',        ' fX$l&ktx^uN.CF/J%(5.4A$l/]#WGUwH$LB;>fK$Nqy8e;6MH;<.OGBl_]!Ez!x');
define('SECURE_AUTH_SALT', 'u:L@N}S9(>4JWIi[afTU[5fX<c/F}n(g?R0I)*+Fp2Vd3^$se`F7&F01K#JfXVp~');
define('LOGGED_IN_SALT',   'RkZCv nsdxPwyCnk$JP5C&ni:0jTf>0>GSNFavH@`,Svky}Tm:377u[3JJuQcy 8');
define('NONCE_SALT',       'o>c$l#c)U>0I?2,7M6%A> 2&oKhP4i,_e?>g.w5,tde,[vB=Dn~]e6%g,Co%r++q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
